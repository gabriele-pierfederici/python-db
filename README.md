A very simple way to interface with databases from python with implementations for SQLite, Oracle and MySQL.

---

###### How to use

```python
from db import SQLiteDatabase, DatabaseError

if __name__ == '__main__':
    db = SQLiteDatabase('test.db')
    try:
        if db.connect():
            db.execute('INSERT INTO contacts (name, email, phone, address) VALUES (?, ?, ?, ?)',
                [(u'John Appleseed', u'j.appleseed@icloud.com', u'(408) 555-0126', u'1 Infinite Loop - 95014 - Cupertino (CA)')])
            db.commit()
            for row in db.rows('SELECT * FROM contacts'):
                print row
            db.disconnect()
    except DatabaseError as e:
        print e
```
