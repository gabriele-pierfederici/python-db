import sqlite3
from database import Database
from errors import DatabaseError

class SQLiteDatabase(Database):
    def __init__(self, database=':memory:'):
        self.database = database

    def _connect(self):
        try:
            return sqlite3.connect(database=self.database)
        except sqlite3.DatabaseError as e:
            raise DatabaseError(e)
