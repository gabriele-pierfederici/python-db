class Error(StandardError):
    pass

class DatabaseError(Error):
    pass
