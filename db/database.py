from errors import DatabaseError

class Database(object):
    __connection = None
    __encoding = None
    __version = None

    def _connect(self):
        pass

    def connect(self):
        if self.__connection is not None:
            raise DatabaseError('Already connected')
        self.__connection = self._connect()
        return self.__connection is not None

    def connection_encoding(self):
        if self.__connection is None:
            raise DatabaseError('Not connected')
        if self.__encoding is None:
            self.__encoding = self.__connection.encoding
        return self.__encoding

    def connection_version(self):
        if self.__connection is None:
            raise DatabaseError('Not connected')
        if self.__version is None:
            self.__version = self.__connection.version
        return self.__version

    def disconnect(self):
        if self.__connection:
            try:
                self.__connection.close()
                self.__connection = None
                self.__version = None
                return True
            except Exception as e:
                raise DatabaseError(e)
        return False

    def commit(self):
        if self.__connection is None:
            raise DatabaseError('Not connected')
        try:
            self.__connection.commit()
        except Exception as e:
            raise DatabaseError(e)

    def rollback(self):
        if self.__connection is None:
            raise DatabaseError('Not connected')
        try:
            self.__connection.rollback()
        except Exception as e:
            raise DatabaseError(e)

    def execute(self, statement, parameters=[]):
        if self.__connection is None:
            raise DatabaseError('Not connected')
        cur = None
        try:
            cur = self.__connection.cursor()
            cur.executemany(statement, parameters)
            cur.close()
            cur = None
        except Exception as e:
            raise DatabaseError(e)
        finally:
            if cur:
                try:
                    cur.close()
                except Exception as e:
                    raise DatabaseError(e)


    def rows(self, statement, parameters=()):
        if self.__connection is None:
            raise DatabaseError('Not connected')
        cur = None
        rows = ()
        try:
            cur = self.__connection.cursor()
            res = cur.execute(statement, parameters)
            if res:
                rows = cur.fetchall()
            cur.close()
            cur = None
        except Exception as e:
            raise DatabaseError(e)
        finally:
            if cur:
                try:
                    cur.close()
                except Exception as e:
                    raise DatabaseError(e)
        return rows
