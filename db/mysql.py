import MySQLdb
from database import Database
from errors import DatabaseError

class MySQLDatabase(Database):
    def __init__(self, user, password, db, host='localhost', charset='utf8mb4'):
        self.user     = user
        self.password = password
        self.db       = db
        self.host     = host
        self.charset  = charset

    def _connect(self):
        try:
            return MySQLdb.connect(user=self.user, password=self.password,
                    db=self.db, host=self.host, charset=self.charset)
        except MySQLdb.DatabaseError as e:
            raise DatabaseError(e)
