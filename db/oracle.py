import cx_Oracle
from database import Database
from errors import DatabaseError

class OracleDatabase(Database):
    def __init__(self, user, password, dsn, cclass="HOL.CLASS",
            purity=cx_Oracle.ATTR_PURITY_SELF):
        self.user     = user
        self.password = password
        self.dsn      = dsn
        self.cclass   = cclass
        self.purity   = purity

    def _connect(self):
        try:
            return cx_Oracle.connect(user=self.user, password=self.password,
                    dsn=self.dsn, cclass=self.cclass, purity=self.purity)
        except cx_Oracle.DatabaseError as e:
            raise DatabaseError(e)
